extern crate tera;

#[macro_use]
extern crate lazy_static;
extern crate serde_json;
extern crate cssoptiadvisor;

use std::fs::{remove_dir_all, File};
use std::error::Error;
use std::process::Command;
use std::collections::HashSet;
use std::path::Path;

use dircpy::copy_dir;
use walkdir::WalkDir;
use tera::{Context, Tera};
use html_minifier::HTMLMinifierHelper;
use colored::Colorize;

use cssoptiadvisor::parse_file;

lazy_static! {
    pub static ref TEMPLATES: Tera = {
        remove_dir_all("templates_tmp").unwrap();
        let _ = copy_dir("templates", "templates_tmp");

        for entry in WalkDir::new("templates_tmp") {
            let _entry = entry.unwrap();
            if let Some(extension) = _entry.path().extension() {
                if extension == "svg" {
                    Command::new("svgo").args([_entry.path(), ])
                    .output()
                    .expect("failed to execute process");
                }
            }
        }
        let mut tera = match Tera::new("templates_tmp/**/*") {
            Ok(t) => t,
            Err(e) => {
                println!("Parsing error(s): {}", e);
                ::std::process::exit(1);
            }
        };
        tera.autoescape_on(vec!["html", ".sql"]);
        tera
    };
}

fn main() {
    let context = Context::new();
    let mut results: HashSet<String> = HashSet::new();
    for path in TEMPLATES.get_template_names() {
        if path.ends_with(".css") {
            let _ = parse_file(Path::new(&format!("templates/{path}")), &mut results);
        }
        if !path.ends_with(".html") {
            continue;
        }
        println!("Name: {}", path);
        match TEMPLATES.render(&path, &context) {
            Ok(s) => {
                let mut f = File::create(format!("result/{}", path)).unwrap();

                /* Minifier */
                let mut html_minifier_helper = HTMLMinifierHelper::new();
                html_minifier_helper.digest(s, &mut f).unwrap();
            },
            Err(e) => {
                println!("Error: {}", e);
                let mut cause = e.source();
                while let Some(e) = cause {
                    println!("Reason: {}", e);
                    cause = e.source();
                }
            }
        };
    }
    for result in results {
        println!("{}", result.white().on_red());
    }
}