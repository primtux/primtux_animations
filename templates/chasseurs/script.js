function animate() {
    document.getElementById('content').classList.remove('hidden');
    let svgElement = document.getElementById('svg1');
    svgElement.classList.remove('animation');
    svgElement.style.transition = 'none';
    svgElement.offsetHeight;
    svgElement.classList.add('animation');
}

document.addEventListener("DOMContentLoaded", function(event) {
    let svgElement = document.getElementById('svg1');

    svgElement.addEventListener("animationend", () => {
        svgElement.classList.remove('animation');
    });
});
