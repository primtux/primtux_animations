# Primutx animations

## Introduction

Créer des animations éducatives pour le projet Primtux.

## cahier des charges

- Il faut que ce soit ludique (destiné à des enfants).
- Objectif pédagogique : l'enfant doit apprendre, comprendre un concept.
- Rapide à créer et modifier : l'objectif est pas de faire du pixar super chiadé : on est une petite équipe bénévole avec peu de moyens.
- Léger : taille du fichier, occupation en ram, cpu/gpu.
- Réalisé uniquement avec des outils libres : svg fait sous Inkscape et le reste via visual studio code.

## Outils du dev

Pour optimiser les fichiers générés :

1. avoir Npm :

```sh
sudo apt-get install npm
```

2. Installer svgo :
```sh
sudo npm install -g svgo
```

3. Le build :
```sh
cargo run
```

On utilise actuellement :

- svgo pour le SVG
- html-minifier pour le html, css, javascript

## Des pistes d'amélioration ?

1. Utiliser Parcel :

```sh
sudo npm install -g parcel
sudo npm i -D @swc/cli @swc/core
```

Exemple :
```sh
parcel build result/chasseurs/chasseurs.html --dist-dir build
```

Pour l'instant, Parcel ne change rien au résultat finale.

2. Utiliser svgcleaner : https://github.com/RazrFalcon/svgcleaner

3. Utiliser swc indépendamment pour le javascript : https://swc.rs/

4. Utiliser htmlnano pour le html

5. Utiliser `lightningcss` pour le css + `cssoptiadvisor`

6. Ajouter des contrôles pour (TODO) :

- Eviter les valeurs identiques dans les @keyframes :
    ex: si 0% et 10% ont la même valeur, il faut par conséquent les concaténer.
- warning : évité les keyframes avec des valeurs trop petites.
    ex: si l'animation dure 1 seconde et que l'on utilise une valeur avec plus de 2 decimales, ça sous-entend qu'on édite de l'animation plus fine que le centième de seconde.
    Le navigateur n'est pas capable de gérer ce niveau de précision et l'oeil humain n'est pas capable de le percevoir.

7. Enlever tous les retours à la ligne

Pour faire ça, il faudrait utiliser cssparser : https://github.com/servo/rust-cssparser
